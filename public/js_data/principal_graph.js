fetch('./principal_activity.json')
.then((response)=>response.json())
.then(function(data){
    const sorted_key=Object.keys(data).sort((a,b)=>data[b]-data[a]);
    const key=[];
    const no_of_reg=[];
    for(let i=0;i<10;i++)
    {
        key.push(sorted_key[i]);
        no_of_reg.push(data[sorted_key[i]]);
    }
    Highcharts.chart('container3',{
        chart:{
            type:'column'
        },
        title:{
            text:'Top registrations by "Principal Business Activity" for the year 2015'
        },
        xAxis:{
            categories:key,
        },
        yAxis:{
            title:{
                text:'Number of Company Registrations',
            }
        },
        series:[{
            name:'Number of Company Registrations',
            //data:[data[0],data[1],data[2],data[3],data[4]],
            data:no_of_reg,
            //colorByPoint:true,
        }]
    });
});
  