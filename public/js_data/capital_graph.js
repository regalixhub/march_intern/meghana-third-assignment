fetch('./authorized_capital_graph.json')
.then((response)=>response.json())
.then(function(data){
    Highcharts.chart('container1',{
        chart:{
            type:'column'
        },
        title:{
            text:'Authorized Capital'
        },
        xAxis:{
            categories:['<1L','1L to 10L','10L to 1Cr','10Cr to 100Cr','More than 100Cr']
        },
        yAxis:{
            title:{
                text:'Authorized_Capital in Rupees'
            }
        },
        series:[{
            name:'Authorized_Capital',
            //data:[data[0],data[1],data[2],data[3],data[4]],
            data,
            //colorByPoint:true,
        }]
    });
});
  