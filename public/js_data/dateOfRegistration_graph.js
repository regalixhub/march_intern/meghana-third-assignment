//import { version } from "punycode";

fetch('./date_of_registration.json')
.then((response)=>response.json())
.then(function(data){
    const key=Object.keys(data);
    const value=Object.values(data);

    Highcharts.chart('container2',{
        chart:{
            type:'column'
        },
        title:{
            text:'NO_OF_COMPANY_REGISTERED_BY_YEAR'
        },
        xAxis:{
            categories:key,
            text:'YEAR',
        },
        yAxis:{
            title:{
                text:'NO_OF_COMPANY_REGISTERED'
            }
        },
        series:[{
            name:'COMPANY_REGISTERED_YEAR',
            //data:[data[0],data[1],data[2],data[3],data[4]],
            data:value,
            //colorByPoint:true,
        }]
    });
});
  